/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package vista.chat;

import comunes.Audio;
import comunes.Comunicaciones;
import java.io.EOFException;
import java.io.File;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JOptionPane;
import persistencia.Config;
import persistencia.GestorFicheros;
import persistencia.Registrador;

/**
 * Hilo receptor de mensajes entrantes y gestor de desconexión remota
 * @author Esteban
 */
class Receptor extends Thread {
    private final Comunicaciones comunicaciones;
    private final String nickRem;
    private final SimpleDateFormat sdf;
    private final Registrador registrador;
    private final Chat chat;
    private EnviadorFicheros enviadorFicheros;

    /**
     * Constructor parametrizado simple
     * @param comunicaciones Gestor de comunicaciones
     * @param chat Ventana de chat
     * @param nickRem Nick del usuario remoto
     * @param registrador Registrador de conversaciones
     */
    public Receptor(Comunicaciones comunicaciones, Chat chat, String nickRem, Registrador registrador) {
        this.comunicaciones = comunicaciones;
        this.nickRem = nickRem;
        this.registrador = registrador;
        this.chat = chat;
        
        sdf = new SimpleDateFormat(Config.getInstancia().getStringProperty("formato_hora"));
    }

    /**
     * Establece un EnviadorFicheros al que comunicar la respuesta de aceptación
     * de archivo
     * @param enviadorFicheros 
     */
    public void setEnviadorFicheros(EnviadorFicheros enviadorFicheros) {
        this.enviadorFicheros = enviadorFicheros;
    }
    
    @Override
    /**
     * Método inicial del hilo. Permanece a la espera de mensajes entrantes.
     * Notifica al usuario y deshabilita el envío de mensajes cuando
     * se pierde la conexión con el equipo remoto.
     * Distingue tres tipos de mensajes recibidos:
     * - String: mensaje de texto normal
     * - File: metainformación de fichero entrante
     * - Boolean: confirmación de envío de fichero
     */
    public void run() {
        Audio audio = new Audio();
        try {
            while (true) {
                Object recibido = comunicaciones.recibir();
                
                if (recibido.getClass().equals(String.class)) {  // Si es un mensaje
                    String linea = sdf.format(Calendar.getInstance().getTime())
                            + " [" + nickRem + "] >> " + (String) recibido + "\n";
                    chat.getTxtChat().append(linea);
                    
                    audio.reproducirPlup();
                    
                    if (chat.registrarConversacion())
                        registrador.registrarLinea(linea);
                    
                } else if (recibido.getClass().equals(File.class)) {  // Si es un fichero
                    audio.reproducirPlup();
                    String nombreFichero = ((File) recibido).getName();
                    
                    // Dialogo de confirmación de descarga
                    if (JOptionPane.showConfirmDialog(chat, nickRem + " te quiere pasar " +
                            nombreFichero + " ¿Descargar?", "Fichero entrante", 
                            JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                        comunicaciones.enviar(Boolean.TRUE);  // Acepta el fichero
                        
                        byte[] contenido = (byte[]) comunicaciones.recibir();
                        String nombreGuardado = GestorFicheros.guardarFichero(chat, nombreFichero, contenido);
                        if (nombreGuardado != null) {  // Si decide guardar el fichero
                            String notificacion = Comunes.generarNotificacion(nombreGuardado + " guardado");
                            chat.getTxtChat().append(notificacion);
                            if (chat.registrarConversacion())
                                registrador.registrarLinea(notificacion);
                        }
                    } else {
                        comunicaciones.enviar(Boolean.FALSE);  // Rechaza el fichero
                    }
                // Si hay un enviador de ficheros y el objeto recibido es un booleano
                } else if (enviadorFicheros != null && recibido.getClass().equals(Boolean.class)) {
                    enviadorFicheros.setAceptado((Boolean) recibido);
                    enviadorFicheros = null;
                }
            }
        } catch (EOFException | SocketException e) {  // Si pierde la conexión con el chat remoto
            chat.setEntradaActiva(false);
            
            String notificacion = Comunes.generarNotificacion(nickRem + " se ha ido a por tabaco :(");
            chat.getTxtChat().append(notificacion);
            if (chat.registrarConversacion())
                registrador.registrarLinea(notificacion);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
