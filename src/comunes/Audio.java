/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package comunes;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * Clase que gestiona los sonidos
 * @author Esteban
 */
public class Audio {
    private Clip chatEntrante;
    
    /**
     * Inicia un loop infinito con el sonido de chat entrante
     */
    public void loopChatEntrante() {
        try {
            chatEntrante = AudioSystem.getClip();
            AudioInputStream ais = AudioSystem.getAudioInputStream(
                    getClass().getResource("/chatEntrante.wav"));
            chatEntrante.open(ais);
            chatEntrante.loop(Clip.LOOP_CONTINUOUSLY);
            chatEntrante.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Detiene (si existe) el loop de chat entrante
     */
    public void detenerLoop() {
        if (chatEntrante != null) {
            chatEntrante.stop();
            chatEntrante = null;
        }
    }
    
    /**
     * Reproduce el sonido de notificación de mensaje entrante
     */
    public void reproducirPlup() {
        try {
            Clip clip = AudioSystem.getClip();
            AudioInputStream ais = AudioSystem.getAudioInputStream(
                    getClass().getResource("/plup.wav"));
            clip.open(ais);
            clip.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
