/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package persistencia;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Clase que permite guardar y leer conversaciones de ficheros 
 * de texto plano. Cada día se usa un fichero diferente
 * @author Esteban
 */
public class Registrador {
    private final String nick;
    private final String nickRem;
    private final File directorio;
    
    /**
     * Constructor que crea el directorio de conversaciones si no existe
     * @param nick Nick del usuario local
     * @param nickRem Nick del usuario remoto
     */
    public Registrador(String nick, String nickRem) {
        this.nick = nick;
        this.nickRem = nickRem;
        this.directorio = new File(Config.getInstancia().getStringProperty("dir_conversaciones"));
        
        // Comprueba que el directorio de conversaciones exista
        if (!directorio.isDirectory())
            directorio.mkdir();
    }
    
    /**
     * Método que registra una linea en el fichero del día actual
     * @param linea Linea a registrar
     */
    public synchronized void registrarLinea(String linea) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
        Calendar cal = Calendar.getInstance();
        
        File fichero = new File(directorio, nick + "-" + nickRem + "_" + sdf.format(cal.getTime()) + ".txt");
        
        try (BufferedWriter fEscritura = new BufferedWriter(new FileWriter(fichero, true))) {
            fEscritura.write(linea);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Método que recupera la conversación del día actual
     * @return Conversación del día actual
     */
    public String recuperarConversacion() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
        Calendar cal = Calendar.getInstance();
        
        File fichero = new File(directorio, nick + "-" + nickRem + "_" + sdf.format(cal.getTime()) + ".txt");
        String conversacion = "", linea;
        
        try (BufferedReader fLectura = new BufferedReader(new FileReader(fichero))) {
            while ((linea = fLectura.readLine()) != null)
                conversacion += linea + "\n";
        } catch (FileNotFoundException e) { 
            // No existe el fichero, everything is OK
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return conversacion;
    }
}
