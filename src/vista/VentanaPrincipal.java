/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package vista;

import servidor.Servidor;
import cliente.Cliente;
import persistencia.Contacto;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import persistencia.Config;
import persistencia.ContactoDAO;

/**
 * Ventana principal del programa; permite realizar conexiones a otros equipos
 * y/o inicia el servidor
 * @author Esteban
 */
public class VentanaPrincipal extends javax.swing.JFrame {
    private final String nick;
    private final DefaultListModel modeloContactos;
    private final ContactoDAO contactoDAO;
    
    /**
     * Constructor que establece los elementos de la interfaz de la ventana 
     * e inicia el servidor (en un hilo propio)
     * @param nick nick del usuario local
     * @param tipoConexion Qué elementos de la conexión iniciar
     */
    public VentanaPrincipal(String nick, TipoConexion tipoConexion) {
        this.nick = nick;
        contactoDAO = new ContactoDAO();
        
        initComponents();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        lblNick.setText(nick);
        // Puerto por defecto
        txtPuerto.setText(Config.getInstancia().getStringProperty("puerto"));
        modeloContactos = new DefaultListModel();
        lstContactos.setModel(modeloContactos);
        cargarContactos();
        
        // Favicon
        try {
            setIconImage(ImageIO.read(this.getClass().getResource("/favicon.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        // Desactiva elementos de la interfaz del modo cliente
        if (tipoConexion == TipoConexion.SOLO_SERVIDOR) {
            panelConectar.setEnabled(false);
            btnConectar.setEnabled(false);
            txtNickRem.setEditable(false);
            txtHostRem.setEditable(false);
        }
        
        // Inicia el servidor
        if (tipoConexion == TipoConexion.AMBOS || tipoConexion == TipoConexion.SOLO_SERVIDOR)
            new Servidor(this, nick).start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelConectar = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNickRem = new javax.swing.JTextField();
        txtHostRem = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtPuerto = new javax.swing.JTextField();
        btnConectar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        lblNick = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstContactos = new javax.swing.JList<>();
        btnBorrar = new javax.swing.JButton();
        btnAnnadir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PonyChat");
        setResizable(false);

        panelConectar.setBorder(javax.swing.BorderFactory.createTitledBorder("Conectar"));

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel1.setText("Nick");

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setText("Host");

        txtNickRem.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        txtHostRem.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel3.setText("Nombre");

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel4.setText("Puerto");

        txtNombre.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        txtPuerto.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        javax.swing.GroupLayout panelConectarLayout = new javax.swing.GroupLayout(panelConectar);
        panelConectar.setLayout(panelConectarLayout);
        panelConectarLayout.setHorizontalGroup(
            panelConectarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConectarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelConectarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3))
                .addGap(3, 3, 3)
                .addGroup(panelConectarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNickRem, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                    .addComponent(txtHostRem)
                    .addComponent(txtNombre)
                    .addComponent(txtPuerto))
                .addContainerGap())
        );
        panelConectarLayout.setVerticalGroup(
            panelConectarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConectarLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelConectarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelConectarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNickRem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelConectarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(txtHostRem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panelConectarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnConectar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnConectar.setText("Conectar");
        btnConectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConectarActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        lblNick.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblNick.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNick.setText("Nick");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Contactos"));

        lstContactos.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        lstContactos.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstContactosValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(lstContactos);

        btnBorrar.setText("Borrar");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });

        btnAnnadir.setText("Añadir");
        btnAnnadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnnadirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnBorrar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                        .addComponent(btnAnnadir)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBorrar)
                    .addComponent(btnAnnadir))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblNick, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(btnConectar)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnSalir))
                        .addComponent(panelConectar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblNick)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelConectar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnConectar)
                            .addComponent(btnSalir))))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * ActionListener del botón de salir
     * @param evt 
     */
    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnSalirActionPerformed

    /**
     * ActionListener del botón Conectar. Deshabilita el botón e inicia un hilo
     * si los JTextFields estás cumplimentados. En caso contrario, notifica al usuario
     * con un cliente nuevo
     * @param evt 
     */
    private void btnConectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConectarActionPerformed
        if (!txtNickRem.getText().isEmpty() && !txtHostRem.getText().isEmpty() && !txtPuerto.getText().isEmpty()) {
            btnConectar.setEnabled(false);
            new Cliente(this, txtHostRem.getText(), txtNickRem.getText(), 
                    Integer.parseInt(txtPuerto.getText()), nick).start();
        } else {
            JOptionPane.showMessageDialog(this, "Campos vacíosclien",
                    "High Quality Error Message", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnConectarActionPerformed

    /**
     * ActionListener del botón Annadir. Si los cuadros de texto no están vacíos,
     * inserta el contacto
     * @param evt 
     */
    private void btnAnnadirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnnadirActionPerformed
        if (!txtNickRem.getText().isEmpty() && !txtHostRem.getText().isEmpty() &&
                !txtNombre.getText().isEmpty() && !txtPuerto.getText().isEmpty()) {
            // Si se inserta
            if (contactoDAO.insertar(new Contacto(txtNombre.getText(), txtNickRem.getText(),
                    txtHostRem.getText(), Integer.parseInt(txtPuerto.getText())), nick))
                modeloContactos.addElement(txtNickRem.getText());
        }
    }//GEN-LAST:event_btnAnnadirActionPerformed

    /**
     * ActionListener del botón borrar. Si hay un nick seleccionado en la lista,
     * lo borra de la lista de contactos
     * @param evt 
     */
    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        if (lstContactos.getSelectedValue() != null) {
            contactoDAO.borrar(lstContactos.getSelectedValue(), nick);
            modeloContactos.remove(lstContactos.getSelectedIndex());
        }
    }//GEN-LAST:event_btnBorrarActionPerformed

    /**
     * Gestor de evento de cambio de valor de lstContactos. Establece en los JTextFields 
     * los datos del contacto seleccionado
     * @param evt 
     */
    private void lstContactosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstContactosValueChanged
        if (lstContactos.getSelectedValue() != null) {
            Contacto c = contactoDAO.obtener(lstContactos.getSelectedValue(), nick);
            txtNickRem.setText(c.getNick());
            txtHostRem.setText(c.getHost());
            txtNombre.setText(c.getNombreCompleto());
            txtPuerto.setText(Integer.toString(c.getPuerto()));
        }
    }//GEN-LAST:event_lstContactosValueChanged

    /**
     * Habilita o deshabilita el botón Conectar
     * @param enable true habilita el botón Conectar
     */
    public void btnConectarEnable(boolean enable) {
        btnConectar.setEnabled(enable);
    }
    
    /**
     * Carga la lista de contactos en lstContactos
     */
    private void cargarContactos() {
        for (Contacto c : contactoDAO.obtener(nick))
            modeloContactos.addElement(c.getNick());
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnnadir;
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnConectar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblNick;
    private javax.swing.JList<String> lstContactos;
    private javax.swing.JPanel panelConectar;
    private javax.swing.JTextField txtHostRem;
    private javax.swing.JTextField txtNickRem;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPuerto;
    // End of variables declaration//GEN-END:variables
}
