/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * DAO de Contacto en SQLite
 * @author Esteban
 */
public class ContactoDAO {
    private static final String URL_BBDD = "jdbc:sqlite:contactos.db";
    public static final int MAX_NICK = 20;
    public static final int MAX_NOMBRE = 40;
    public static final int MAX_HOST = 20;
    private static final String SQL_INSERTAR = "INSERT INTO contactos (nick, nombreCompleto, host, puerto, nickLocal) VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_BORRAR = "DELETE from contactos WHERE nick=? AND nickLocal=?";
    
    /**
     * Constructor que crea la tabla de contactos si no existe ya
     */
    public ContactoDAO() {
        try {
            // Crea la tabla de contactos si no existe
            try (Connection conexion = obtenerConexion()) {
                PreparedStatement preparedStatement = conexion.prepareStatement("CREATE TABLE IF NOT EXISTS contactos ("
                        + "nick VARCHAR(" + MAX_NICK + "),"
                        + "nombreCompleto VARCHAR(" + MAX_NOMBRE + "),"
                        + "host VARCHAR(" + MAX_HOST + "),"
                        + "puerto INT,"
                        + "nickLocal VARCHAR(" + MAX_NICK + "))");
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    private Connection obtenerConexion() throws SQLException {
        return DriverManager.getConnection(URL_BBDD);
    }
    
    /**
     * Ejecuta una consulta con o sin filtro (WHERE)
     * @param filtro Filtro WHERE opcional
     * @return Array de Contactos obtenido de la consulta
     * @throws SQLException si ocurre un error al ejecutar la orden
     */
    private Contacto[] ejecutarQuery(String filtro) throws SQLException {
        ArrayList<Contacto> contactos = new ArrayList<>();
        String query = "SELECT * from contactos";
        if (filtro != null)
            query += " WHERE " + filtro;
        
        try (Connection conexion = obtenerConexion()) {
            PreparedStatement consulta = conexion.prepareStatement(query);
            ResultSet rs = consulta.executeQuery();
            while (rs.next()) {
                contactos.add(new Contacto(rs.getString("nombreCompleto"), 
                        rs.getString("nick"), rs.getString("host"), rs.getInt("puerto")));
            }
        }
        
        return contactos.toArray(new Contacto[0]);
    }
    
    /**
     * Inserta un contacto si no estaba registrado ya
     * @param contacto Contacto a insertar
     * @param nickLocal nick del usuario logueado localmente
     * @return true si no existía ya el contacto
     */
    public boolean insertar(Contacto contacto, String nickLocal) {
        try {
            // Si no hay registros con el mismo nick
            if (ejecutarQuery("nick=\"" + contacto.getNick() + "\"").length == 0) {
                try (Connection conexion = obtenerConexion()) {
                    PreparedStatement preparedStatement = conexion.prepareStatement(SQL_INSERTAR);
                    preparedStatement.setString(1, contacto.getNick());
                    preparedStatement.setString(2, contacto.getNombreCompleto());
                    preparedStatement.setString(3, contacto.getHost());
                    preparedStatement.setInt(4, contacto.getPuerto());
                    preparedStatement.setString(5, nickLocal);
                    preparedStatement.executeUpdate();
                }
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        return false;
    }
    
    /**
     * Borra un contacto
     * @param nickGuardado Nick del usuario a borrar
     * @param nickLocal nick del usuario logueado localmente
     */
    public void borrar(String nickGuardado, String nickLocal) {
        try {
            try (Connection conexion = obtenerConexion()) {
                PreparedStatement preparedStatement = conexion.prepareStatement(SQL_BORRAR);
                preparedStatement.setString(1, nickGuardado);
                preparedStatement.setString(2, nickLocal);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    /**
     * Devuelve el Contacto de un nick
     * @param nickGuardado Nick a buscar
     * @param nickLocal nick del usuario logueado localmente
     * @return Contacto encontrado. Null si no encuentra el contacto
     */
    public Contacto obtener(String nickGuardado, String nickLocal) {
        Contacto c = null;
        
        try {
            Contacto[] array = ejecutarQuery("nick=\"" + nickGuardado + "\" AND nickLocal=\"" + nickLocal + "\"");           
            if (array.length > 0)
                c = array[0];
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        return c;
    }
    
    /**
     * Devuelve un array con los contactos registrados
     * @param nickLocal nick del usuario logueado localmente
     * @return Array de contactos. Null si hay error
     */
    public Contacto[] obtener(String nickLocal) {
        Contacto[] contactos = null;
        
        try {
            contactos = ejecutarQuery("nickLocal=\"" + nickLocal + "\"");
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        return contactos;
    }
}
