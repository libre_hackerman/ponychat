/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

package vista.chat;

import comunes.Comunicaciones;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.imageio.ImageIO;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;
import persistencia.Config;
import persistencia.Registrador;

/**
 * Ventana de chat
 * @author Esteban
 */
public class Chat extends javax.swing.JFrame {
    private final String nick;
    private final Comunicaciones comunicaciones;
    private final Receptor receptor;
    private final SimpleDateFormat sdf;
    private final Registrador registrador;
    
    /**
     * Constructor parametrizado que inicia los componentes de la ventana
     * y abre el hilo receptor de mensajes
     * @param nick Nick del usuario local
     * @param nickRem Nick del usuario remoto
     * @param comunicaciones Gestor de comunicaciones
     */
    public Chat(String nick, String nickRem, Comunicaciones comunicaciones) {
        this.nick = nick;
        this.comunicaciones = comunicaciones;
        
        initComponents();
        setTitle("Chat con " + nickRem);
        lblNick.setText(nick);
        
        
        // Favicon
        try {
            setIconImage(ImageIO.read(this.getClass().getResource("/favicon.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        // Autoscroll
        DefaultCaret caret = (DefaultCaret)txtChat.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        
        sdf = new SimpleDateFormat(Config.getInstancia().getStringProperty("formato_hora"));
        
        registrador = new Registrador(nick, nickRem);
        txtChat.setText(registrador.recuperarConversacion());
                
        receptor = new Receptor(comunicaciones, this, nickRem, registrador);
        receptor.start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        txtChat = new javax.swing.JTextArea();
        txtMensaje = new javax.swing.JTextField();
        btnEnviar = new javax.swing.JButton();
        lblNick = new javax.swing.JLabel();
        btnFichero = new javax.swing.JButton();
        chkbRegConv = new javax.swing.JCheckBox();

        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        txtChat.setEditable(false);
        txtChat.setColumns(20);
        txtChat.setLineWrap(true);
        txtChat.setRows(5);
        jScrollPane1.setViewportView(txtChat);

        txtMensaje.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        txtMensaje.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMensajeKeyPressed(evt);
            }
        });

        btnEnviar.setText("Enviar");
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });

        lblNick.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblNick.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNick.setText("Nick");

        btnFichero.setText("Enviar archivo...");
        btnFichero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFicheroActionPerformed(evt);
            }
        });

        chkbRegConv.setSelected(true);
        chkbRegConv.setText("Registrar conversación");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 504, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblNick, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 354, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnEnviar))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(chkbRegConv)
                        .addGap(18, 18, 18)
                        .addComponent(btnFichero)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 354, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFichero)
                    .addComponent(chkbRegConv))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEnviar)
                    .addComponent(lblNick))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Gestor del evento de cierre de la ventana de chat. Interrumpe el hilo
     * receptor, cierra los flujos y socket y libera los recursos de la ventana
     * @param evt 
     */
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try {
            receptor.interrupt();
            comunicaciones.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dispose();
    }//GEN-LAST:event_formWindowClosing

    /**
     * ActionListener del botón Enviar. Manda el mensaje de txtMensaje, lo
     * añade a txtChat, lo guarda y limpia txtMensaje
     * @param evt 
     */
    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
        try {
            comunicaciones.enviar(txtMensaje.getText());
            
            String linea = sdf.format(Calendar.getInstance().getTime()) + 
                    " [" + nick + "] >> " + txtMensaje.getText() + "\n";
            txtChat.append(linea);
            
            if (chkbRegConv.isSelected())
                registrador.registrarLinea(linea);
            
            txtMensaje.setText("");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnEnviarActionPerformed

    /**
     * Gestor de evento de tecla pulsada de txtMensaje. Si se pulsa Enter,
     * simula la pulsación del botón Enviar
     * @param evt Evento de tecla
     */
    private void txtMensajeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMensajeKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER)
            btnEnviarActionPerformed(null);
    }//GEN-LAST:event_txtMensajeKeyPressed

    /**
     * Método que inicia el proceso de envío de un fichero. Se hace en un
     * hilo aparte para no bloquear el chat durante el proceso
     * @param evt 
     */
    private void btnFicheroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFicheroActionPerformed
        EnviadorFicheros enviadorFicheros = new EnviadorFicheros(this, comunicaciones, registrador);
        receptor.setEnviadorFicheros(enviadorFicheros);  // Recibirá la confirmación del envío de fichero
        enviadorFicheros.start();
    }//GEN-LAST:event_btnFicheroActionPerformed
    
    /**
     * Método para bloquear/desbloquear la entrada de información al usuario
     * @param activo true desbloquea, false bloquea
     */
    public void setEntradaActiva(boolean activo) {
        btnEnviar.setEnabled(activo);
        btnFichero.setEnabled(activo);
        txtMensaje.setEditable(activo);
    }

    /**
     * Getter del JTextArea del chat
     * <i> Es synchronized para evitar conflictos entre el Receptor y el
     * EnviadorFicheros</i>
     * @return JTextArea del chat
     */
    public synchronized JTextArea getTxtChat() {
        return txtChat;
    }
    
    /**
     * Indica si el checkbox de registrar conversación está marcado
     * @return true Si el checkbox de registrar conversación está marcado
     */
    public boolean registrarConversacion() {
        return chkbRegConv.isSelected();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEnviar;
    private javax.swing.JButton btnFichero;
    private javax.swing.JCheckBox chkbRegConv;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblNick;
    private javax.swing.JTextArea txtChat;
    private javax.swing.JTextField txtMensaje;
    // End of variables declaration//GEN-END:variables
}