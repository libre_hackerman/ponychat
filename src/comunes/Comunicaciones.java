/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package comunes;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Clase que encapsula y gestiona el socket y los flujos de lectura/escritura
 * @author Esteban
 */
public class Comunicaciones implements Closeable {
    private Socket socket;
    private ObjectInputStream fEntrada;
    private ObjectOutputStream fSalida;
    
    /**
     * Constructor que recibe un socket y genera los flujos de lectura/escritura
     * @param socket Socket con el equipo remoto
     */
    public Comunicaciones(Socket socket) {
        this.socket = socket;
        
        try {
            fSalida = new ObjectOutputStream(socket.getOutputStream());
            fEntrada = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    /**
     * Envía un objeto a través del socket
     * @param ob Objecto a enviar
     * @throws IOException
     */
    public void enviar(Object ob) throws IOException {
        fSalida.writeObject(ob);
    }
    
    /**
     * Recibe un objeto del socket
     * @return Objecto recibido
     * @throws Exception 
     */
    public Object recibir() throws Exception {
        return fEntrada.readObject();
    }
    
    /**
     * Método que devuelve la dirección del host del socket
     * @return Dirección del host del socket
     */
    public String getSocketAdress() {
        return socket.getInetAddress().getHostAddress();
    }

    @Override
    /**
     * Cierra los flujos y el socket
     */
    public void close() throws IOException {
        fEntrada.close();
        fSalida.close();
        socket.close();
    }
    
}
