/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package vista.chat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import persistencia.Config;

/**
 * Clase con métodos de utilidad en varias clases del chat
 * @author Esteban
 */
public class Comunes {
    /**
     * Genera una notificación <i>decorando</i> un mensaje
     * @param mensaje Mensaje a decorar
     * @return La notificación
     */
    public static String generarNotificacion(String mensaje) {
        SimpleDateFormat sdf = new SimpleDateFormat(Config.getInstancia().getStringProperty("formato_hora"));
        
        String separador = "";
        for (int i = 0; i < Config.getInstancia().getIntProperty("num_separadores"); i++)
            separador += Config.getInstancia().getCharProperty("char_separador");
        
        return separador + "\n" + sdf.format(Calendar.getInstance().getTime()) + 
                "\n" + mensaje + "\n" + separador + "\n";
    }
}
