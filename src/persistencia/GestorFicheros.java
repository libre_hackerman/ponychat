/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package persistencia;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

/**
 * Clase gestora de la carga/descarga de archivos a memoria
 * @author Esteban
 */
public class GestorFicheros {
    
    /**
     * Método que selecciona un fichero con un JFileChooser y obtiene un array
     * cuyo primer elemento es el File del fichero y el segundo un array de bytes
     * con su contenido. Si el usuario no selecciona ningún fichero, devuelve null
     * @param padre Ventana padre del JFileChooser
     * @return Paquete del fichero o null
     */
    public static Object[] cargarFichero(JFrame padre) {
        Object[] paquete = null;
        File fichero;
        
        JFileChooser explorador = new JFileChooser();
        explorador.setFileSelectionMode(JFileChooser.FILES_ONLY);

        if (explorador.showDialog(padre, "Enviar") == JFileChooser.APPROVE_OPTION) {
            fichero = explorador.getSelectedFile();
            
            try {
                byte[] contenido = Files.readAllBytes(fichero.toPath());
                paquete = new Object[2];
                paquete[0] = fichero;
                paquete[1] = contenido;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        return paquete;
    }
    
    /**
     * Método que escribe un array de bytes en un fichero, elegido mediante
     * un JFileChooser
     * @param padre Ventana padre del JFileChooser
     * @param nombreFichero Nombre por defecto del fichero
     * @param contenido Contenido del fichero en array de bytes
     * @return Nombre del fichero en el que se guarda
     */
    public static String guardarFichero(JFrame padre, String nombreFichero, byte[] contenido) {
        File fichero = new File(System.getProperty("user.home"), nombreFichero);
        
        String nombreFicheroDefinitivo = null;
        JFileChooser explorador = new JFileChooser();
        explorador.setFileSelectionMode(JFileChooser.FILES_ONLY);
        explorador.setSelectedFile(fichero);

        if (explorador.showDialog(padre, "Guardar") == JFileChooser.APPROVE_OPTION) {
            fichero = explorador.getSelectedFile();
            
            try {
                Files.write(fichero.toPath(), contenido);
                nombreFicheroDefinitivo = fichero.getName();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        return nombreFicheroDefinitivo;
    }
}
