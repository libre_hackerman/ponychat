/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package cliente;

import comunes.Comunicaciones;
import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.swing.JOptionPane;
import persistencia.Config;
import vista.chat.Chat;
import vista.VentanaPrincipal;

/**
 * Hilo que inicia la conexión con el servidor remoto y (si acepta la petición)
 * abre un chat con él
 * @author Esteban
 */
public class Cliente extends Thread {
    private final String hostRem, nickRem, nick;
    private final int puerto;
    private final VentanaPrincipal ventanaPrincipal;

    /**
     * Constructor parametrizado simple
     * @param ventanaPrincipal Ventana principal del programa
     * @param hostRem Host remoto
     * @param nickRem Nick del usuario remoto
     * @param puerto Puerto remoto
     * @param nick Nick del usuario local
     */
    public Cliente(VentanaPrincipal ventanaPrincipal, String hostRem, String nickRem, int puerto, String nick) {
        this.hostRem = hostRem;
        this.nickRem = nickRem;
        this.puerto = puerto;
        this.nick = nick;
        this.ventanaPrincipal = ventanaPrincipal;
    }
    
    @Override
    /**
     * Método inicial del hilo. Inicia el socket y los flujos, manda al servidor
     * el nick del usuario remoto y el local y espera su respuesta. Si es
     * afirmativa, abre un chat. Si es negativa, cierra el gestor de comunicaciones
     * y notifica al usuario
     */
    public void run() {
        try {
            Comunicaciones comunicaciones = new Comunicaciones(new Socket(hostRem, puerto));
            
            comunicaciones.enviar(nickRem);  // Manda el nick solicitado
            comunicaciones.enviar(nick);  // Manda el nick propio
            
            if ((boolean) comunicaciones.recibir()) {
                new Chat(nick, nickRem, comunicaciones).setVisible(true);
            } else {
                comunicaciones.close();
                
                JOptionPane.showMessageDialog(ventanaPrincipal, hostRem + " ha declinado el parlamento",
                    "Forever Alone", JOptionPane.ERROR_MESSAGE);
            }
            
            // Reactiva el botón de Conectar una vez ha obtenido la respuesta
            ventanaPrincipal.btnConectarEnable(true);
        } catch (NoRouteToHostException | ConnectException e) { 
            JOptionPane.showMessageDialog(ventanaPrincipal, "No se pudo conectar con " + hostRem,
                    "Error de conexión", JOptionPane.ERROR_MESSAGE);
            ventanaPrincipal.btnConectarEnable(true);
        } catch (UnknownHostException e) {
            JOptionPane.showMessageDialog(ventanaPrincipal, "No conozco a ningún " + hostRem,
                    "Host desconocido", JOptionPane.ERROR_MESSAGE);
            ventanaPrincipal.btnConectarEnable(true);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
