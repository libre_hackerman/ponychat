/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package servidor;

import com.dosse.upnp.UPnP;
import comunes.Audio;
import comunes.Comunicaciones;
import java.net.ServerSocket;
import javax.swing.JOptionPane;
import java.net.BindException;
import java.net.SocketException;
import persistencia.Config;
import vista.chat.Chat;
import vista.VentanaPrincipal;

/**
 * Servidor que permanece a la espera de conexiones entrantes. Cuando recibe
 * una, ofrece abrir un chat y vuelve a esperar más conexiones
 * @author Esteban
 */
public class Servidor extends Thread {
    private final VentanaPrincipal ventanaPrincipal;
    private final String nick;
    private ServerSocket serverSocket;
    private Comunicaciones comunicaciones;
    private boolean enShutdownHook;
    
    /**
     * Constructor parametrizado simple
     * @param ventanaPrincipal Ventana principal del programa
     * @param nick nick del usuario local
     */
    public Servidor(VentanaPrincipal ventanaPrincipal, String nick) {
        this.ventanaPrincipal = ventanaPrincipal;
        this.nick = nick;
        enShutdownHook = false;
    }
    
    /**
     * Establece un shutdownHook que cerrará el serverSocket al cierre
     * de la JVM
     */
    private void cierreSocketServerFinal() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    enShutdownHook = true;
                    serverSocket.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        });
    }
    
    /**
     * Muestra un cuadro de diálogo preguntando si se acepta la conexión.
     * En caso afirmativo, abre un cliente en un hilo propio.
     * En caso negativo, cierra el socket y los flujos
     */
    private void conexionEntrante() {
        Audio audio = new Audio();
        try {
            String nickRem = (String) comunicaciones.recibir();
            audio.loopChatEntrante();
            int resp = JOptionPane.showConfirmDialog(ventanaPrincipal, nickRem + " [" + 
                    comunicaciones.getSocketAdress() + "] quiere parlamentar contigo. ¿Te hace?",
                    "Conexión entrante", JOptionPane.YES_NO_OPTION);
            audio.detenerLoop();
            
            if (resp == JOptionPane.YES_OPTION) {
                comunicaciones.enviar(Boolean.TRUE);  // Acepta la petición
                
                // Abre chat
                new Thread() {
                    @Override
                    public void run() {
                        new Chat(nick, nickRem, comunicaciones).setVisible(true);
                    }
                }.start();
            } else {
                comunicaciones.enviar(Boolean.FALSE);  // Deniega la petición
                comunicaciones.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    @Override
    /**
     * Método inicial del hilo. Permanece a la espera de conexiones entrantes.
     * Cuando recibe una, llama a <b>conexionEntrante()</b>
     */
    public void run() {
        
        // Intenta abrir el puerto con UPnP
        if (Config.getInstancia().getCharProperty("upnp") == '1')
            new Thread(abrirUpnp).start();
        
        int puerto = Config.getInstancia().getIntProperty("puerto");
        try {
            serverSocket = new ServerSocket(puerto);
            cierreSocketServerFinal();
            
            boolean coinciden;
            String nickSolicitado;
            while (true) {
                do {
                    comunicaciones = new Comunicaciones(serverSocket.accept());
                    nickSolicitado = (String) comunicaciones.recibir();

                    if (!(coinciden = nick.equals(nickSolicitado))) {
                        comunicaciones.enviar(Boolean.FALSE);  // Deniega la petición
                        comunicaciones.close();
                    }
                } while (!coinciden);
                conexionEntrante();
            }
            
        } catch (BindException e) {
            JOptionPane.showMessageDialog(ventanaPrincipal, "Ya hay un servidor usando el puerto " +
                puerto , "Error del servidor", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        } catch (SocketException e) {
            if (!enShutdownHook) {
                e.printStackTrace();
                System.exit(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    /**
     * Este Runnable intenta abrir el puerto TCP por UPnP
     */
    private final Runnable abrirUpnp = new Runnable() {
        @Override
        public void run() {
            if (UPnP.isUPnPAvailable()) {
                int puerto = Config.getInstancia().getIntProperty("puerto");
                
                if (UPnP.isMappedTCP(puerto)) {
                    System.out.println("Puerto " + puerto + " ya estaba abierto");
                } else if (UPnP.openPortTCP(puerto)) {
                    System.out.println("Puerto " + puerto + " abierto por UPnP");
                } else {
                    System.err.println("Error al intentar abrir el puerto " + 
                            puerto + " con UPnP");
                }
            } else {
                System.err.println("UPnP no disponible");
            }
        }
    };
}
