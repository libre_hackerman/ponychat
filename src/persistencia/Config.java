/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package persistencia;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * Clase singleton que gestiona el fichero de configuración
 * @author Esteban
 */
public class Config {
    private static Config instancia;
    private static final String FICHERO = "config.properties";
    private Properties properties;
    
    /**
     * Constructor que carga el fichero de configuración. Si no existe,
     * establece las propiedades por defecto y escribe el fichero
     */
    private Config() {
        properties = new Properties();
        
        try {
            properties.load(new FileReader(FICHERO));
        } catch (FileNotFoundException e) {
            // Carga la configuración por defecto
            properties.setProperty("puerto", "40020");
            properties.setProperty("upnp", "1");
            properties.setProperty("formato_hora", "dd-MM-yy $ HH:mm:ss");
            properties.setProperty("char_separador", "=");
            properties.setProperty("num_separadores", "20");
            properties.setProperty("dir_conversaciones", "conversaciones");
            properties.setProperty("fichero_contactos", "contactos.txt");
            
            try {
                // Guarda el fichero
                properties.store(new FileWriter(FICHERO), "Configuración por defecto");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    /**
     * Método getInstance de la clase
     * @return Instancia singleton de Config
     */
    public static Config getInstancia() {
        if (instancia == null)
            instancia = new Config();
        
        return instancia;
    }
    
    /**
     * Obtiene una propiedad de tipo número entero
     * @param prop Clave de la propiedad
     * @return Valor de la propiedad
     */
    public int getIntProperty(String prop) {
        int intProp = 0;
        String sProp = properties.getProperty(prop);
        
        if (sProp != null) {
            try {
                intProp = Integer.parseInt(sProp);
            } catch (NumberFormatException e) {
                System.err.println(prop + ": " + properties.getProperty(prop) + " no es un número entero");
                System.exit(1);
            }
        } else {
            System.err.println("No se ha definido la propiedad " + prop);
            System.exit(1);
        }
        
        return intProp;
    }
    
    /**
     * Obtiene una propiedad de tipo carácter
     * @param prop Clave de la propiedad
     * @return Valor de la propiedad
     */
    public char getCharProperty(String prop) {
        char charProp = '\0';
        String sProp = properties.getProperty(prop);
        
        if (sProp != null) {
            charProp = sProp.charAt(0);
        } else {
            System.err.println("No se ha definido la propiedad " + prop);
            System.exit(1);
        }
        
        return charProp;
    }
    
    /**
     * Obtiene una propiedad de tipo cadena
     * @param prop Clave de la propiedad
     * @return Valor de la propiedad
     */
    public String getStringProperty(String prop) {
        String sProp = properties.getProperty(prop);
        
        if (sProp == null) {
            System.err.println("No se ha definido la propiedad " + prop);
            System.exit(1);
        }
        
        return sProp;
    }
}
