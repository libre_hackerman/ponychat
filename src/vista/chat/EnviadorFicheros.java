/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package vista.chat;

import comunes.Comunicaciones;
import java.io.File;
import persistencia.GestorFicheros;
import persistencia.Registrador;

/**
 * Hilo que envía la metainformación y el fichero al equipo remoto
 * @author Esteban
 */
class EnviadorFicheros extends Thread {
    private final Chat chat;
    private final Comunicaciones comunicaciones;
    private Boolean aceptado;
    private final Registrador registrador;

    /**
     * Constructor parametrizado simple
     * @param chat Chat
     * @param comunicaciones Gestor de comunicaciones
     * @param registrador Registrador de conversaciones
     */
    public EnviadorFicheros(Chat chat, Comunicaciones comunicaciones, Registrador registrador) {
        this.chat = chat;
        this.comunicaciones = comunicaciones;
        this.registrador = registrador;
        aceptado = null;
    }
    
    /**
     * Setter de la confirmación de envío
     * @param aceptado true envía el fichero
     */
    public synchronized void setAceptado(Boolean aceptado) {
        this.aceptado = aceptado;
    }
    
    @Override
    /**
     * Método inicial del hilo. Permite al usuario elegir y cargar el archivo, manda
     * el File del archivo al equipo remoto y espera hasta recibir una respuesta.
     * Si es afirmativa, manda el contenido del archivo
     */
    public void run() {
        Object[] paquete = GestorFicheros.cargarFichero(chat);
        if (paquete != null) {
            try {
                // Desactiva la entrada del usuario para no hacer conflicto en el flujo de escritura
                chat.setEntradaActiva(false);
                
                comunicaciones.enviar(paquete[0]);  // Envía el File

                // Espera la respuesta del equipo remoto
                while (aceptado == null) {
                    sleep(500);
                }
                
                if (aceptado) {
                    comunicaciones.enviar(paquete[1]);  // Envía el contenido del fichero
                    String notificacion = Comunes.generarNotificacion(((File) paquete[0]).getName() + " enviado");
                    chat.getTxtChat().append(notificacion);
                    registrador.registrarLinea(notificacion);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                chat.setEntradaActiva(true);
            }
        }
    }
}
